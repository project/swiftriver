The SwiftRiver module provides integration between
Drupal and Swift River and its related services.

http://swift.ushahidi.com/
http://github.com/ushahidi/Swiftriver


API Key
---------------------------------------------------
You will need an API key to use the SwiftRiver services. You
can obtain one from the following page:

http://sws.ushahidi.com/