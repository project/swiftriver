<?php

/**
 * @file
 * This file holds the functions for the main SwiftRiver Admin settings.
 *
 * @ingroup swiftriver
 */

/**
 * Menu callback; displays the swiftriver module settings page.
 *
 * @see system_settings_form()
 */
function swiftriver_admin_settings() {
  // TODO??

  // Define Form
  $form['swiftriver_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Swift River API'),
    '#description' => t('Enter your SWS (SwiftRiver Web Services) API key here.  You can currently get an API key at <a href="@url">@url</a>.', array('@url' => 'http://sws.ushahidi.com/')),
    '#default_value' => variable_get('swiftriver_api_key', ''),
  );

  // Make a system setting form and return
  return system_settings_form($form);
}
