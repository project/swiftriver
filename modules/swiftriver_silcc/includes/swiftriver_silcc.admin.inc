<?php

/**
 * @file
 * This file holds the functions for the main SwiftRiver SiLCC settings.
 *
 * @ingroup swiftriver
 */

/**
 * Menu callback; displays the swiftriver_silcc module settings page.
 *
 * @see system_settings_form()
 */
function swiftriver_silcc_admin_settings() {
  // TODO??

  // Define Form
  $form['swiftriver_silcc_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Swift River SiLCC API Key'),
    '#description' => t('This doesnt do anything.'),
    '#default_value' => variable_get('swiftriver_silcc_api_key', ''),
  );

  // Make a system setting form and return
  return system_settings_form($form);
}
