<?php

/**
 * @file
 * This file holds the functions for the installing
 * and enabling of the swiftriver_sulsa module.
 *
 * @ingroup swiftriver
 */

/**
 * Implements hook_install().
 */
function swiftriver_sulsa_install() {
  drupal_install_schema('swiftriver_sulsa');
}

/**
 * Implements hook_uninstall().
 */
function swiftriver_sulsa_uninstall() {
  drupal_uninstall_schema('swiftriver_sulsa');
  
  // Get global variable array
  global $conf;
  foreach (array_keys($conf) as $key) {
    // Find variables that have the module prefix
    if (strpos($key, 'swiftriver_sulsa_') === 0) {
      variable_del($key);
    }
  }
}

/**
 * Implements hook_schema().
 */
function swiftriver_sulsa_schema() {
  $schema = array();
  
  // Table to store lat and lon from sulsa
  $schema['swiftriver_sulsa']['description'] = 
    'SwiftRiver SULSa table for storing location data.';
  $schema['swiftriver_sulsa']['fields'] = array(
    'sulsa_id' => array(
      'description' => 'The primary identifier for a SULSa location.',
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'lat' => array(
      'description' => 'Latitude of IP of post.',
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ),
    'lon' => array(
      'description' => 'Longitude of IP of post.',
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ),
    'ip' => array(
      'description' => 'The IP of the user that made the update.',
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ),
    'timestamp' => array(
      'description' => 'The Unix timestamp when the location was created.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
    'data' => array(
      'description' => 'A place for other information.',
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
      'serialize' => TRUE,
    ),
  );
  $schema['swiftriver_sulsa']['indexes'] = array(
    'swiftriver_sulsa_ip' => array('ip'),
    'swiftriver_sulsa_created' => array('timestamp'),
  );
  $schema['swiftriver_sulsa']['primary key'] = array('sulsa_id');
  
  // Node table to store lat and lon from sulsa
  $schema['swiftriver_sulsa_node']['description'] = 
    'SwiftRiver SULSa table for storing location data associated with nodes.';
  $schema['swiftriver_sulsa_node']['fields'] = array(
    'nid' => array(
      'description' => 'The {node} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'vid' => array(
      'description' => 'The current {node_revisions}.vid version identifier.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'sulsa_id' => array(
      'description' => 'The {swiftriver_sulsa} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
  );
  $schema['swiftriver_sulsa_node']['indexes'] = array(
    'swiftriver_sulsa_node_sulsa_id' => array('sulsa_id'),
    'swiftriver_sulsa_node_nid' => array('nid'),
  );
  $schema['swiftriver_sulsa_node']['primary key'] = array('vid');
  
  // User table to store lat and lon from sulsa
  $schema['swiftriver_sulsa_user']['description'] = 
    'SwiftRiver SULSa table for storing location data associated with users.';
  $schema['swiftriver_sulsa_user']['fields'] = array(
    'uid' => array(
      'description' => 'The {user} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'sulsa_id' => array(
      'description' => 'The {swiftriver_sulsa} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
  );
  $schema['swiftriver_sulsa_user']['indexes'] = array(
    'swiftriver_sulsa_user_ip'  => array('sulsa_id'),
  );
  $schema['swiftriver_sulsa_user']['primary key'] = array('uid');
  
  // Comment table to store lat and lon from sulsa
  $schema['swiftriver_sulsa_comment']['description'] = 
    'SwiftRiver SULSa table for storing location data associated with comments.';
  $schema['swiftriver_sulsa_comment']['fields'] = array(
    'cid' => array(
      'description' => 'The {comment} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'sulsa_id' => array(
      'description' => 'The {swiftriver_sulsa} id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
  );
  $schema['swiftriver_sulsa_comment']['indexes'] = array(
    'swiftriver_sulsa_comment_ip'  => array('sulsa_id'),
  );
  $schema['swiftriver_sulsa_comment']['primary key'] = array('cid');
  
  return $schema;
}