<?php

/**
 * @file
 * This file holds the functions for the dealing with SULSa and comments.
 *
 * @ingroup swiftriver
 */

/**
 * Load SULSa location comment
 *
 * @param $cid
 *   Comment ID.
 * 
 * @return
 *   Loaded SULSa Object, see swiftriver_sulsa_locate().
 *   Loaded with comment data as well.  If fail, return false.
 */
function swiftriver_sulsa_load_user($cid = NULL) {
  if (!is_numeric($cid) || $cid <= 0) {
    return FALSE;
  }
  
  // Create query and get data
  $query = "
    SELECT * 
    FROM {swiftriver_sulsa_comment} sc
      LEFT JOIN {swiftriver_sulsa} s
        ON sc.sulsa_id = s.sulsa_id
    WHERE sc.cid = %d
  ";
  return db_fetch_object(db_query($query, $cid));
}