<?php

/**
 * @file
 * This file holds the functions for the main SwiftRiver SULSa settings.
 *
 * @ingroup swiftriver
 */

/**
 * Menu callback; displays the swiftriver_sulsa module settings page.
 *
 * @see system_settings_form()
 */
function swiftriver_sulsa_admin_settings() {
  // Message about connecting
  _swiftriver_connect_message();
  
  // User interations
  $user_interactions = array(
    'register' => t('Locate when user registers for the site.'),
    'log_on' => t('Locate when user logs into the site.'),
    'daily' => t('Locate once a day, utilizing sessions.'),
    'page_load' => t('Locate on every page load. Note that this might decrease performance greatly.'),
  );

  // Define Form
  $form['swiftriver_sulsa_user_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Settings'),
    '#description' => t('Settings that define how your site interacts with SULSa and users.  Users can be located by their IP address and have that data stored on your site.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['swiftriver_sulsa_user_settings']['swiftriver_sulsa_user_services'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable User Services'),
    '#description' => t('Define if users location information will be gathered.'),
    '#default_value' => variable_get('swiftriver_sulsa_user_services', FALSE),
  );
  $form['swiftriver_sulsa_user_settings']['swiftriver_sulsa_user_interaction'] = array(
    '#type' => 'checkboxes',
    '#title' => t('How To Locate Users'),
    '#description' => t('Define how users location information will be gathered.'),
    '#options' => $user_interactions,
    '#default_value' => variable_get('swiftriver_sulsa_user_interaction', array()),
  );
  $form['swiftriver_sulsa_node_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Settings'),
    '#description' => t('Settings that define how your site interacts with SULSa and nodes are defined per <a href="@url">content type</a>.',
      array('@url' => url('admin/content/types/list'))
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['swiftriver_sulsa_comment_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment Settings'),
    '#description' => t('Settings that define how your site interacts with SULSa and comments are defined per <a href="@url">content type</a>.',
      array('@url' => url('admin/content/types/list'))
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Make a system setting form and return
  return system_settings_form($form);
}
