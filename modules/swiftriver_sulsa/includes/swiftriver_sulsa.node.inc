<?php

/**
 * @file
 * This file holds the functions for the dealing with SULSa and nodes.
 *
 * @ingroup swiftriver
 */

/**
 * Load SULSa location node
 *
 * @param $vid
 *   Node ID.
 * @param $nid
 *   Use to specifiy specific version number.
 * 
 * @return
 *   Loaded SULSa Object, see swiftriver_sulsa_locate().
 *   Loaded with node data as well.  If fail, return false.
 */
function swiftriver_sulsa_load_node($nid = NULL, $vid = NULL) {
  if (!is_numeric($nid) || $nid <= 0) {
    return FALSE;
  }
  
  // Determine vid
  if (!is_numeric($vid) || $vid <= 0) {
    // Get latest vid for node
    db_result(db_result("SELECT vid FROM {node} WHERE nid = %d", $nid));
  }
  
  // Create query and get data
  $query = "
    SELECT * 
    FROM {swiftriver_sulsa_node} sn 
      LEFT JOIN {swiftriver_sulsa} s
        ON sn.sulsa_id = s.sulsa_id
    WHERE sn.nid = %d AND sn.vid = %d
  ";
  return db_fetch_object(db_query($query, $vid, $nid));
}