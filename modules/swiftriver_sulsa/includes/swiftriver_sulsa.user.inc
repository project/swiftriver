<?php

/**
 * @file
 * This file holds the functions for the dealing with SULSa and user.
 *
 * @ingroup swiftriver
 */

/**
 * Load SULSa location user
 *
 * @param $uid
 *   User ID.
 * 
 * @return
 *   Loaded SULSa Object, see swiftriver_sulsa_locate().
 *   Loaded with user data as well.  If fail, return false.
 */
function swiftriver_sulsa_load_user($uid = NULL) {
  if (!is_numeric($uid) || $uid <= 0) {
    return FALSE;
  }
  
  // Create query and get data
  $query = "
    SELECT * 
    FROM {swiftriver_sulsa_user} su
      LEFT JOIN {swiftriver_sulsa} s
        ON su.sulsa_id = s.sulsa_id
    WHERE su.uid = %d
  ";
  return db_fetch_object(db_query($query, $uid));
}